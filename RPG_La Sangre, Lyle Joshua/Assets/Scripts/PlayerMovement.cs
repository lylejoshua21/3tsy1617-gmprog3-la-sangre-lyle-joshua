﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {

    Animator animator;
    NavMeshAgent navMeshAgent;

    private Vector3 destination;

	// Use this for initialization
	void Start ()
    {
        animator = this.GetComponent<Animator>();
        navMeshAgent = this.GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            destination = Input.mousePosition;

            if(Physics.Raycast(Camera.main.ScreenPointToRay(destination), out hit, 100))
            {
                navMeshAgent.destination = hit.point;
                animator.SetInteger("Idle", 1);
            }
        }
	}
}
