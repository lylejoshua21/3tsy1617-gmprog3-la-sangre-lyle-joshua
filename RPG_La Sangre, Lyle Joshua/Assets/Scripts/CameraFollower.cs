﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    public GameObject player;
    private Vector3 camOffset;
    public float moveSpeed;

	// Use this for initialization
	void Start ()
    {
        camOffset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.Lerp(transform.position, player.transform.position + camOffset, Time.deltaTime * moveSpeed);
	}
}
